import "./js/script.js";
import "./js/slick.js";
import "./fonts/fonts.scss";
import "./styles/main.scss";
import "./styles/index/header.scss";
import "./styles/index/search.scss";
import "./styles/index/person.scss";
import "./styles/index/footer.scss";

import "./styles/person-page/page.scss";

import "./styles/about/slider.scss";