window.jQuery = window.$ = require('jquery/dist/jquery.js');
require('slick-carousel/slick/slick.js');
require('slick-carousel/slick/slick.scss');
require('slick-carousel/slick/slick-theme.scss');

$(function(){

	// плавная прокрутка якоря //

	$('.person__link').on('click', function(e){                                                             
		$('html,body').stop().animate({ scrollTop: $('#person-anchor').offset().top-50 }, 800);
		e.preventDefault();
	});


	// фиксация меню //

	var hightHeader = 172; // высота шапки
	var hightMargin = 0;   // отступ когда шапка уже не видна

	var elem = $('#menu');
    var top = $(this).scrollTop();
     
    if(top > hightHeader){
        elem.css('top', hightMargin);

    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+hightMargin < hightHeader) {
            elem.css('top', (hightHeader-top));
            $(".header__navbar").removeClass("navbar-scroll");

        } else {
            elem.css('top', hightMargin);
            $(".header__navbar").addClass("navbar-scroll");
        }
    });

    // запуск видео при клике на область видео //
    
    $("video").prop("volume", 0.3).click(function(){this[this.paused?"play":"pause"]()});

});

