$(document).ready(function(){
    $('.slick-slider_about').slick({
			  slidesToShow: 1,
			  infinite: true,
			  speed: 800,
			  fade: true,
  			cssEase: 'linear',
  			arrow: true,
        autoplay: true,
  			autoplaySpeed: 3000,
        adaptiveHeight: true
  			/*centerMode: true,
  			variableWidth: true*/
    });

    $('.slick-slider_pp').slick({
        slidesToShow: 1,
        infinite: true,
        speed: 600,
        fade: true,
        cssEase: 'linear',
        arrow: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000
    });
});
 
